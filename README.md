# nuxt-blog

> Проверить работу можно на сайте https://nuxt-js-blog-ssr.herokuapp.com/
чтобы попасть на страницу админа нужно перейти на https://nuxt-js-blog-ssr.herokuapp.com/admin
Логин bitbucket      пароль bitbucket
## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
