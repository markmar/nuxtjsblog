const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require ('mongoose');
const passport = require('passport');

const passportStrategy = require('./middleware/passport-straregy');
const keys = require ('./keys');
const authRoutes = require('./routes/auth.router');
const postRoutes = require('./routes/post.routes');
const commentRoutes = require('./routes/comment.routes');

const app = express();

mongoose.connect(keys.MONGO_URI, {useNewUrlParser: true})
  .then(() => {
    console.log('connect');
  })
  .catch(error => console.error(error) );

app.use(passport.initialize());
passport.use(passportStrategy);
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use('/api/auth', authRoutes);
app.use('/api/post', postRoutes);
app.use('/api/comment', commentRoutes);

module.exports = app;