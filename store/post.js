import { setTimeout } from "timers";

const posts = [
  {title: 'post', date: new Date(), views: 22, comments: [1, 2], _id: '123123123'},
  {title: 'post 2', date: new Date(), views: 22, comments: [1, 2], _id: '123121111'},
];


export const actions = {
  async fetchAdmin ({commit}) {
    try {
      const posts = await this.$axios.$get('/api/post/admin');
      return posts;
    } catch (error) {
      commit('setError', e, {root: true});
      throw error;
    }
  },
  async fetch ({commit}){
    try {
      const posts = await this.$axios.$get('/api/post');
      return posts;
    } catch (error) {
      commit('setError', e, {root: true});
      throw error;
    } 
  },
  async remove ({commit}, id) {
    try {
      const posts = await this.$axios.$delete(`/api/post/admin/${id}`);
      return posts;
    } catch (error) {
      commit('setError', e, {root: true});
      throw error;
    }
  },
  async fetchAdminById ({commit}, id) {
    try {
      const posts = await this.$axios.$get(`/api/post/admin/${id}`);
      return posts;
    } catch (error) {
      commit('setError', e, {root: true});
      throw error;
    }
  },
  async fetchById ({commit}, id) {
    try {
      const posts = await this.$axios.$get(`/api/post/${id}`);
      return posts;
    } catch (error) {
      commit('setError', e, {root: true});
      throw error;
    }
  },
  async update ({commit}, {id, text}) {
    try {
      const posts = await this.$axios.$put(`/api/post/admin/${id}`, {text});
      return posts;
    } catch (error) {
      commit('setError', e, {root: true});
      throw error;
    }
  },
  async create ({commit}, {title, text, image}) {
    try {
      const fd = new FormData();

      fd.append('title', title);
      fd.append('text', text);
      fd.append('image', image, image.name);

      return await this.$axios.$post('/api/post/admin', fd);

    } catch (error) {
      commit('setError', error, {root: true});
      throw error;
    }
    
  },
  async addView({commit}, {views, _id}){
    try {
      const posts = await this.$axios.$put(`/api/post/add/view/${_id}`, {views});
      return posts;
    } catch (error) {
      commit('setError', e, {root: true});
      throw error;
    }
  },

  
  async getAnalytics({commit}) {
    try {
      return await this.$axios.$get('api/post/admin/get/analytics');
    } catch (e) {
      commit('setError', e, {root: true});
      throw e;
    }
  }
};